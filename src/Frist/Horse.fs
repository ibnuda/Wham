module Horse

open System

open RabbitMQ.Client
open RabbitMQ.Client.Events

// https://blog.geist.no/functional-queueing-with-rabbitmq-and-f/
type Queue =
  | PesanDiterima
  | PesanDikirim
